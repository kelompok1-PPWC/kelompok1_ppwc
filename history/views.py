from django.shortcuts import render

# Create your views here.
def history_func(request):
	html = 'history_page.html'
	return render(request, html)
