from django.shortcuts import render
from viewmore.models import Berita, Program

# Create your views here.
def home(request):
	news = Berita.objects.all().values()
	temp = []
	progs = Program.objects.all().values()
	temp2 = []
	html = 'homePage.html'
	

	if news.count() >= 4:
		for x in range(4):
			temp.append(news[x])
	else:
		for x in range(news.count()):
			temp.append(news[x])

	if progs.count() >= 6:
		for x in range(6):
			temp2.append(progs[x])
	else:
		for x in range(progs.count()):
			temp2.append(progs[x])
	

	response = {'progs': temp2, 'news' : temp }
	
	return render(request, html, response)

